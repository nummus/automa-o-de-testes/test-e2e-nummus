import { Before } from '@cucumber/cucumber'
import { beforeFeatureActions, beforeScenarioActions } from './steps.utils.js'

Before(async () => {
  /**
   * Before Feature
   */
  await beforeFeatureActions()

  /**
   * Before Scenario
   */
  await beforeScenarioActions()
})
