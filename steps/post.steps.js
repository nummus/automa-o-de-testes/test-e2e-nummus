import { Then } from '@cucumber/cucumber'
import { context } from '@nummus/nummus-cashback-robot'
import api from '../services/service.js'

Then(/^existe a seguinte informação:$/,
  async (json) => {
    if (!json) return

    await api.posting.fromFile(json)
  })

Then(/^existe a seguinte informação no início (do scenario|da feature):$/,
  async (type, json) => {
    const shouldReturn = type === 'da feature'
      ? !context.get('isNewFeature')
      : !context.get('isNewScenario')

    if (shouldReturn || !json) return

    await api.posting.fromFile(json)
  })

Then(/^existe a seguinte informação se "([^"]*)" for verdadeiro:$/,
  async (shouldReturn, json) => {
    if (!shouldReturn || !json) return

    await api.posting.fromFile(json)
  })
