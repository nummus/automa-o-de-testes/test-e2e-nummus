import { Then } from '@cucumber/cucumber'
import { context, properties, resolvers, utils } from '@nummus/nummus-cashback-robot'
import { putServiceNoId } from '../services/put.js'

Then(/^atualiza a configuração de cashback para:$/, async (json) => {
  const jsonObject = JSON.parse(json)
  const jsonSolved = await resolvers.resolvePlainJson(jsonObject)
  const idEmpresa = properties.get('i.empresa')
  const normalizedUrl = `empresas/${idEmpresa}/cashback-config`

  await putServiceNoId(normalizedUrl, jsonSolved)
})

Then(/^atualiza a seguinte informação em "([^"]*)"$/,
  async (url, data) => {
    if (!url) return

    const normalizedUrl = await utils.service.resolveUrl(url)

    const jsonObject = JSON.parse(data)
    const jsonSolved = await resolvers.resolvePlainJson(jsonObject)

    await putServiceNoId(normalizedUrl, jsonSolved)
  })

Then(/^atualiza a seguinte informação em "([^"]*)" no início da feature$/,
  async (url, data) => {
    if (!context.get('isNewFeature') || !url) return

    const normalizedUrl = await utils.service.resolveUrl(url)

    const jsonObject = JSON.parse(data)
    const jsonSolved = await resolvers.resolvePlainJson(jsonObject)

    await putServiceNoId(normalizedUrl, jsonSolved)
  })

Then(/^atualiza a seguinte informação em "([^"]*)" no início do scenario$/,
  async (url, data) => {
    if (!context.get('isNewScenario') || !url) return

    const normalizedUrl = await utils.service.resolveUrl(url)

    const jsonObject = JSON.parse(data)
    const jsonSolved = await resolvers.resolvePlainJson(jsonObject)

    await putServiceNoId(normalizedUrl, jsonSolved)
  })
