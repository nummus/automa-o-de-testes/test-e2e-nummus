import { Then } from '@cucumber/cucumber'
import { resolvers, utils } from '@nummus/nummus-cashback-robot'
import chalk from 'chalk'

/**
 * Utilizado para debug
 */
Then(/^testa a resolução do json:$/,
  async (json) => {
    const jsonData = JSON.parse(json)

    for (const pathJson of jsonData) {
      const { filePath } = pathJson
      const jsonData = await resolvers.getJsonFileData(`json/${filePath}`)
      const solvedJson = await resolvers.resolvePlainJson(jsonData)

      utils.logger.info('info', 'solvedJson', solvedJson)
    }
  })

/**
 * Utilizado para debug
 */
Then(/^salvo o log do navegador$/,
  async () => {
    await browser.getLog('browser', function (logEntriesArray) {
      console.log('Log length: ' + logEntriesArray.length)

      logEntriesArray.forEach(function (log) {
        console.log('[' + log.level + '] ' + log.timestamp + ' : ' + log.message)
      })
    })
  })

/**
 * Utilizado para debug
 */
Then(/^realizo o log do texto do elemento "([^"]*)"."([^"]*)"$/, async (page, element) => {
  if (!page || !element) return

  const textFound = []
  const selector = utils.element.getPageSelector(page, element)
  const resultElements = await browser.elements('css selector', selector)

  if (!resultElements.length) return

  const webdriverIds = utils.element.extractWebdriverId(resultElements)

  for (const webdriverId of webdriverIds) {
    textFound.push(await browser.elementIdText(webdriverId))
  }

  console.log('')
  console.log(chalk.green.bold('→ → Text found:'), textFound.join(' '))
  console.log('')
})
