import { Then } from '@cucumber/cucumber'
import { context, properties, utils } from '@nummus/nummus-cashback-robot'

Then(/^acesso a url "([^"]*)" da nummus$/,
  async url => {
    if (!url) return

    const normalizedUrl = await utils.service.resolveUrl(url)
    const urlLaunch = properties.get('url.launch')
    const urlToGo = `${urlLaunch}${normalizedUrl}`

    await browser.url(urlToGo)

    await utils.action.waitForLoadingNotPresent()
  })

Then(/^acesso a url "([^"]*)" da nummus no início (da feature|do scenario)$/,
  async (url, startOption) => {
    const startType = startOption === 'da feature'
      ? !context.get('isNewFeature')
      : !context.get('isNewScenario')

    if (startType || !url) return

    const normalizedUrl = await utils.service.resolveUrl(url)
    const urlLaunch = properties.get('url.launch')
    const urlToGo = `${urlLaunch}${normalizedUrl}`

    const urlNummus = urlToGo

    await browser.url(urlNummus)

    await utils.action.waitForLoadingNotPresent()
  })

Then(/^aguardo a mensagem com o texto "(.*)"$/,
  async text => {
    if (!text) return

    const selector = '.toast-container'

    await browser.waitForElementText(selector, text, { typeText: false })
  })

Then(/^aguarda a mensagem não estar presente$/,
  async () => {
    const selector = '.toast-container'

    await browser.waitForElementNotPresent(selector)
  })

Then(/^aguardo e fecho a mensagem com o texto "(.*)"$/,
  async text => {
    if (!text) return

    const selector = '.toast-container'

    await browser
      .waitForElementText({ selector, text, isEquals: false })
      .click(selector)
      .waitForElementNotPresent(selector)
  })
