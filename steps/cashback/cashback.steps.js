import { When } from '@cucumber/cucumber'
import { resolvers, utils } from '@nummus/nummus-cashback-robot'
import { elements as cashbackPage } from '../../pages/cashback.page.js'

When(/^informo os seguintes produtos "([^"]*)" no cashback$/,
  async filePath => {
    if (!filePath) return

    const cashbackSelectors = utils.element.simplifyPage(cashbackPage)
    const produtos = await resolvers.resolveJson(filePath)

    const {
      botaoAdicionaProduto, selecionaProduto,
      opcoesDeProdutos, campoSelecionarProduto, campoValorProduto
    } = cashbackSelectors

    for (let index = 0; index < produtos.length; index++) {
      let resultElements = null
      let resultElementsOpcoes = null

      // Clica no botão + de produto a partir do segundo item
      if (index) {
        await browser
          .waitForElementPresent(botaoAdicionaProduto)
          .click(botaoAdicionaProduto)
          .pause(1000)
      }

      // Seleciona o campo produto
      await browser
        .waitForElementPresent(selecionaProduto)
        .click({ selector: selecionaProduto, index })

      // Buscando o elemento pai e filho das opções de produto para clicar
      await browser
        .waitForElementPresent(campoSelecionarProduto)
        .elements('css selector', campoSelecionarProduto, result => {
          resultElements = result
        })
        .pause(1000)

      const webdriverIds = utils.element.extractWebdriverId(resultElements)

      await browser.elementIdElements(webdriverIds[index], 'css selector', opcoesDeProdutos, result => {
        resultElementsOpcoes = result
      })

      const webdriverIdsOpcoes = utils.element.extractWebdriverId(resultElementsOpcoes)

      for (const webdriver of webdriverIdsOpcoes) {
        let webdriverText = null

        await browser.elementIdText(webdriver, result => {
          webdriverText = result
        })

        const valorEnviado = produtos[index].nomeProduto.toUpperCase()
        const valorEncontrado = webdriverText.value.toUpperCase()

        if (valorEnviado !== valorEncontrado) continue

        await browser.elementIdClick(webdriver)

        break
      }

      await browser.pause(1000)

      // Informa o valor do produto
      await browser
        .waitForElementPresent(campoValorProduto)
        .updateValue({ selector: campoValorProduto, index }, produtos[index].valor)
    }
  })
