import { Then } from '@cucumber/cucumber'
import { context, utils } from '@nummus/nummus-cashback-robot'
import api from '../services/service.js'

Then(/^excluo a informação em "([^"]*)"$/,
  async (url) => {
    if (!url) return

    const urlMap = await utils.service.resolveUrl(url)

    try {
      await api.deleting.allTheSameTime({ url: urlMap })
    } catch (err) {
      console.error(err)
    }
  })

Then(/^excluo a informação em "([^"]*)" se "([^"]*)" for verdadeiro$/,
  async (url, start) => {
    if (!url || !start) return

    const urlMap = await utils.service.resolveUrl(url)

    try {
      await api.deleting.allTheSameTime({ url: urlMap })
    } catch (err) {
      console.error(err)
    }
  })

Then(/^excluo a informação em "([^"]*)" com o filtro "([^"]*)"$/,
  async (url, filter) => {
    if (!url) return

    const urlMap = await utils.service.resolveUrl(url)

    try {
      await api.deleting.allTheSameTime({ url: urlMap, filter })
    } catch (err) {
      console.error(err)
    }
  })

Then(/^excluo a informação em "([^"]*)" no início (da feature|do scenario)$/,
  async (url, startOption) => {
    const startType = startOption === 'da feature'
      ? !context.get('isNewFeature')
      : !context.get('isNewScenario')

    if (startType || !url) return

    const urlMap = await utils.service.resolveUrl(url)

    try {
      await api.deleting.allTheSameTime({ url: urlMap })
    } catch (err) {
      console.error(err)
    }
  })

Then(/^excluo a informação em "([^"]*)" com o filtro "([^"]*)" no início (da feature|do scenario)$/,
  async (url, filter, startOption) => {
    const startType = startOption === 'da feature'
      ? !context.get('isNewFeature')
      : !context.get('isNewScenario')

    if (startType || !url) return

    const urlMap = await utils.service.resolveUrl(url)

    try {
      await api.deleting.allTheSameTime({ url: urlMap, filter })
    } catch (err) {
      console.error(err)
    }
  })
