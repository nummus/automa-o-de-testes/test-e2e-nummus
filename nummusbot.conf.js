require('@babel/register')()

module.exports = {
  assertionsPath: './assertions',
  commandsPath: './commands',
  featuresPath: './features',
  filesPath: './files',
  pagesPath: './pages',
  reportsPath: './reports',
  resolversPath: './resolvers',
  screenshotsPath: './reports/screenshots',
  servicesPath: './services',
  stepsPath: './steps'
}
