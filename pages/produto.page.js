const { selectOptionSelector } = require("@nummus/nummus-cashback-robot/lib/utils/action.utils");

module.exports = {
  elements: {
    botaoAdicionarProduto: {
      selector: '#produtos_servicos_adicionar_produto'
    },
    inputCadastroCodigoProduto: {
      selector: '.product-input #produtos_servicos_input_code'
    },
    inputCadastroDescricaoProduto: {
      selector: '.product-input #produtos_servicos_input_name'
    },
    camposConteudoProduto: {
      selector: '.product-content input'
    },
    selecionaTipoValidade: {
      selector: '#produtos_servicos_input_validity_type'
    },
    botaoRemover: {
      selector: '.fa-trash-can'
    },
    botaoSalvar: {
      selector: '.modal-nummus-container button[label=salvar]'
    },
    botaoAtualizar: {
      selector: '.refresh > button'
    }
  }
}
