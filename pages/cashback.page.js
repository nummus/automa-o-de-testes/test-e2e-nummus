module.exports = {
  elements: {
    botaoBonus: {
      selector: '#cashback_bonus'
    },
    botaoNovoCashback: {
      selector: '#cashback_novo_cashback'
    },
    cardsDeTotal: {
      selector: '.total-content'
    },
    cardTotalVenda: {
      selector: '.total-content:nth-child(1)'
    },
    cardTotalCobrar: {
      selector: '.total-content:nth-child(2)'
    },
    cardTotalCashback: {
      selector: '.total-content:nth-child(3)'
    },
    camposCashback: {
      selector: '.input-nummus-container input'
    },
    avatarCliente: {
      selector: '.customer-photo .avatar-nummus-container'
    },
    selecionaProduto: {
      selector: '#item'
    },
    campoValorProduto: {
      selector: '#item_value'
    },
    botaoGerarCashback: {
      selector: '#cashback_novo_button_gerar_cashback'
    },
    botaoResgatar: {
      selector: '#cashback_novo_resgatar'
    },
    botaoCancelarResgatar: {
      selector: '#cashback_novo_cancelar_resgate'
    },
    campoValorResgate: {
      selector: '#cashback-value-rescue'
    },
    textoValorResgateCashback: {
      selector: '.card-launch.rescue .title'
    },
    resgateDisponivel: {
      selector: '#amount-customer'
    },
    resgateMaximo: {
      selector: '#max-rescue'
    },
    botaoAdicionaProduto: {
      selector: '#button_add_remove_items[data-type="add"]'
    },
    botaoRemoveProduto: {
      selector: '#button_add_remove_items[data-type="remove"]'
    },
    opcoesDeProdutos: {
      selector: '.filter-select-option'
    },
    campoSelecionarProduto: {
      selector: '.filter-select-nummus-container .input-cashback-launch'
    },
    continuaLancandoCashback: {
      selector: '#button-keep-launch-cashback'
    }
  }
}
