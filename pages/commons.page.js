module.exports = {
  elements: {
    button: {
      selector: 'button'
    },
    listLine: {
      selector: 'table tr'
    },
    opcaoDeSelecao: {
      selector: '.filter-select-option'
    },
    mensagem: {
      selector: '.toast-container'
    },
    botaModalConfirma: {
      selector: '.modal-content .confirm'
    },
    botaModalCancela: {
      selector: '.modal-content .cancel'
    },
    menuNovoCashback: {
      selector: '[label=Cashback]'
    }
  }
}
