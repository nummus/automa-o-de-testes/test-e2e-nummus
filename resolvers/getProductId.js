import { properties, utils } from '@nummus/nummus-cashback-robot'
import Axios from 'axios'

export default async function getProductId (productCode, status = 'A') {
  /**
   * Get the url from properties
   */

  const urlToGet = `${properties.get('url.api')}company/products`
  const filters = `{"code":"${productCode}","status":"${status}"}`

  console.log('urlToGet :>> ', urlToGet)

  /**
   * Searching data, strategies change depending of the filter variable or qUrls variable
   */
  try {
    const { data } = await Axios.get(urlToGet,
      {
        params: { filters },
        headers: { empresa: properties.get('i.codigo') }
      })

    if (!data) return null

    if (data?.content?.length) {
      if (!data) return data.content[0].id

      return data.content.map(object => object.id)[0]
    }

    if (data?.length) return data[0].id
  } catch (error) {
    const err = { error, functionName: 'getProductId' }

    await utils.report.robotErrorHandler(err)
  }
}
