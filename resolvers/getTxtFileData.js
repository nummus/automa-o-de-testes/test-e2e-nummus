import { utils } from '@nummus/nummus-cashback-robot'
import fs from 'fs'
import { join, resolve } from 'path'

export default function getTxtFileData (filePath) {
  const txtPath = join(resolve(utils.base.getConfig('filesPath')), filePath)

  utils.logger.debug('resolver', 'getTxtFileData() txt path:', txtPath)

  const txtData = fs.readFileSync(txtPath, 'utf-8')

  utils.logger.debug('resolver', 'getTxtFileData() txt data:', txtData)

  return txtData
}
