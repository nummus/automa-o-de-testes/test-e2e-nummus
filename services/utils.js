import { resolvers, utils } from '@nummus/nummus-cashback-robot'

export const doShiftIds = async (urlTarget, idsInOrder) => {
  try {
    const urlsToShift = await resolvers.getJsonFileData('/properties/url_ids_to_shift.json')

    for (const data of urlsToShift) {
      const urlResult = await utils.service.resolveUrl(data.url)

      if (urlTarget !== urlResult) continue

      for (let index = 0; index < data.quantity; index++) {
        idsInOrder.shift()
      }
    }

    return idsInOrder
  } catch (error) {
    throw new Error('doShiftIds', error)
  }
}
