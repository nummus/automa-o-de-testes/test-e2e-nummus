import { properties, utils } from '@nummus/nummus-cashback-robot'
import Axios from 'axios'
import jp from 'jsonpath'
import { getServiceAll, getServiceFilter } from './get.js'
import { doShiftIds } from './utils.js'

const deleting = {
  oneAtTime: async (infoToPerform) => {
    await performDelete(infoToPerform)
  },
  allTheSameTime: async (infoToPerform) => {
    await performDeleteConcurrently(infoToPerform)
  },
  filterWithJsonPath: async (infoToPerform) => {
    await performDeleteLocalFilter(infoToPerform)
  },
  notUsingId: async (url) => {
    await performDeleteNotUseId(url)
  },
  usingCompleteUrl: async (url) => {
    await performDeleteWithUrl(url)
  },
  basicDelete: async (url, ids) => {
    await deleteService(url, ids)
  },
  basicDeleteConcurrently: async (url, ids) => {
    await deleteConcurrently(url, ids)
  }
}

export const deleteService = async (url, ids) => {
  const IdInOrder = await ids.sort((a, b) => { return b - a })

  for (const id of IdInOrder) {
    const urlWithId = `${properties.get('url.api')}${url}/${id}`

    try {
      await Axios.delete(urlWithId, {
        headers: { empresa: properties.get('i.codigo') }
      })
    } catch (error) {
      const err = { error, functionName: 'deleteService' }

      await utils.report.robotErrorHandler(err)
    }
  }
}

export const deleteConcurrently = async (url, ids) => {
  const deleteService = async (urlWithId) => {
    try {
      await Axios.delete(urlWithId, {
        headers: { empresa: properties.get('i.codigo') }
      })
    } catch (error) {
      const err = { error, functionName: 'deleteConcurrently' }
      await utils.report.robotErrorHandler(err)
    }
  }

  const urls = ids.map(id => `${properties.get('url.api')}${url}/${id}`)

  await Promise.all(urls.map(deleteService))
}

export const performDelete = async (infoToPerform) => {
  let { url, filter, urlGet, limit = 40 } = infoToPerform

  let data = null
  let contentFilter = null

  const urlMap = await utils.service.resolveUrl(url)

  if (urlGet) urlGet = await utils.service.resolveUrl(urlGet)

  urlGet
    ? !filter
        ? (data = await getServiceAll(urlGet, limit))
        : (data = await getServiceFilter(urlGet, filter, limit))
    : !filter
        ? (data = await getServiceAll(urlMap, limit))
        : (data = await getServiceFilter(urlMap, filter, limit))

  if (!data) return

  data.content ? (contentFilter = data.content) : (contentFilter = data)

  if (!contentFilter) return

  if (!Array.isArray(contentFilter)) {
    contentFilter = [contentFilter]
  }

  const ids = await contentFilter.map((item) => item.id)
  let idsInOrder = await ids.sort((a, b) => { return a - b })

  idsInOrder = await doShiftIds(urlMap, idsInOrder)

  console.log('idsInOrder :>> ', idsInOrder)

  if (!idsInOrder || !idsInOrder.length) return

  await deleteService(urlMap, idsInOrder)

  if (!data.last && data.last !== undefined) {
    await deleting.oneAtTime({ urlMap, filter, urlGet, limit })
  }
}

export const performDeleteLocalFilter = async (infoToPerform) => {
  const { url, filter = '$.*', limit = 40 } = infoToPerform
  const urlMap = await utils.service.resolveUrl(url)

  const data = await getServiceAll(urlMap, limit)

  if (!data) return

  const contentFilter = await jp.query(data.content, filter)

  if (!contentFilter) return

  const ids = await contentFilter.map((item) => item.id)
  let idsInOrder = await ids.sort((a, b) => { return a - b })

  idsInOrder = await doShiftIds(urlMap, idsInOrder)

  if (!idsInOrder || !idsInOrder.length) return

  await deleteService(url, ids)
}

export const performDeleteConcurrently = async (infoToPerform) => {
  let { url, filter, urlGet, limit = 40 } = infoToPerform

  if (!url) return

  let data = null
  let contentFilter = null

  const urlMap = await utils.service.resolveUrl(url)

  if (urlGet) urlGet = await utils.service.resolveUrl(urlGet)

  urlGet
    ? !filter
        ? (data = await getServiceAll(urlGet, limit))
        : (data = await getServiceFilter(urlGet, filter, limit))
    : !filter
        ? (data = await getServiceAll(urlMap, limit))
        : (data = await getServiceFilter(urlMap, filter, limit))

  if (!data) return

  data.content ? (contentFilter = data.content) : (contentFilter = data)

  if (!contentFilter) return

  if (!Array.isArray(contentFilter)) {
    contentFilter = [contentFilter]
  }

  const ids = await contentFilter.map((item) => item.id)
  let idsInOrder = await ids.sort((a, b) => { return a - b })

  idsInOrder = await doShiftIds(urlMap, idsInOrder)

  if (!idsInOrder || !idsInOrder.length) return

  await deleteConcurrently(urlMap, idsInOrder)

  if (!data.last && data.last !== undefined) {
    await deleting.allTheSameTime({ urlMap, filter, urlGet, limit })
  }
}

export const performDeleteWithUrl = async (url) => {
  try {
    await Axios.delete(`${url}`, {
      headers: { empresa: properties.get('i.codigo') }
    })
  } catch (error) {
    const err = { error, functionName: 'performDeleteWithUrl', statusSkip: 404 }

    await utils.report.robotErrorHandler(err)
  }
}

export const performDeleteNotUseId = async (url) => {
  const urlToDelete = `${properties.get('url.api')}${url}`

  try {
    await Axios.delete(urlToDelete, {
      headers: { empresa: properties.get('i.codigo') }
    })
  } catch (error) {
    const err = { error, functionName: 'performDeleteNotUseId', statusSkip: 404 }

    await utils.report.robotErrorHandler(err)
  }
}

export default deleting
