import { properties, utils } from '@nummus/nummus-cashback-robot'
import Axios from 'axios'

export const getServiceFilter = async (url, filter, value, limit = 50, offset = 0) => {
  if (typeof url !== 'string' || typeof filter !== 'string' || typeof limit !== 'number') {
    throw new Error('Invalid input parameters')
  }

  const urlToGet = `${properties.get('url.api')}${url}`

  if (url === 'empresa/cashback') {
    const limitOffset64 = btoa(`{"limit":${limit},"offset":${offset}},"filter_by":${filter}`)

    try {
      const { data } = await Axios.get(urlToGet,
        {
          params: { credentials: limitOffset64 },
          headers: { empresa: properties.get('i.codigo') }
        })

      return data
    } catch (error) {
      const err = { error, functionName: 'getServiceFilter' }

      await utils.report.robotErrorHandler(err)
    }
  } else {
    try {
      const { data } = await Axios.get(urlToGet,
        {
          params: { limit, offset, [filter]: value },
          headers: { empresa: properties.get('i.codigo') }
        })

      return data
    } catch (error) {
      const err = { error, functionName: 'getServiceAll' }

      await utils.report.robotErrorHandler(err)
    }
  }
}

export const getServiceAll = async (url, limit = 50, offset = 0) => {
  let limitOffset64
  const urlToGet = `${properties.get('url.api')}${url}`

  if (url === 'empresa/cashback') {
    limitOffset64 = btoa(`{"limit":${limit},"offset":${offset}}`)

    try {
      const { data } = await Axios.get(urlToGet,
        {
          params: { credentials: limitOffset64 },
          headers: { empresa: properties.get('i.codigo') }
        })

      return data
    } catch (error) {
      const err = { error, functionName: 'getServiceAll' }

      await utils.report.robotErrorHandler(err)
    }
  } else {
    try {
      const { data } = await Axios.get(urlToGet,
        {
          params: { limit, offset },
          headers: { empresa: properties.get('i.codigo') }
        })

      return data
    } catch (error) {
      const err = { error, functionName: 'getServiceAll' }

      await utils.report.robotErrorHandler(err)
    }
  }
}
