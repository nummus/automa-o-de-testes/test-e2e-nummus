import { properties } from '@nummus/nummus-cashback-robot'
import ora from 'ora'

export const standardizeCompany = async () => {
  if (properties.get('disable.interaction')) return

  console.log('')
  const spinConfig = ora('Iniciando configuração da empresa nummus...').start()

  try {
    spinConfig.text = 'Configurando...'

    spinConfig.succeed('Nummus configurado com sucesso.')
  } catch (error) {
    spinConfig.fail('Erro na configuração da empresa nummus.')

    throw error
  }

  console.log('')
  const spinDelete = ora('Limpando e atualizando os dados da empresa...').start()

  try {
    spinDelete.text = 'Limpando dados de produtos...'
    // await api.deleting.oneAtTime({ url: 'produto_service' })

    spinDelete.succeed('Dados da empresa limpos e atualizados.')
  } catch (error) {
    spinDelete.fail(`Erro ao limpar e atualizar os dados da empresa: ${spinDelete.text}`)

    throw error
  }

  if (properties.get('nightwatch.detailed.output')) console.log('')
}
