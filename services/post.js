import { properties, resolvers, utils } from '@nummus/nummus-cashback-robot'
import Axios from 'axios'
import ora from 'ora'

const posting = {
  fromFile: async (jsonToPost) => {
    await postJsonFileToApi(jsonToPost)
  },
  fromJson: async (infoToPost) => {
    await postJsonToApi(infoToPost)
  },
  fromString: async (url, string) => {
    await postJsonStringToApi(url, string)
  },
  fromObject: async (url, object) => {
    await postObjectToApi(url, object)
  }
}

export const postJsonFileToApi = async (json) => {
  let jsonToPost = json
  const spinConfig = ora('Iniciando o envio dos dados para a nummus...').start()

  try {
    if (typeof json === 'string') {
      jsonToPost = JSON.parse(json)
    }

    if (!Array.isArray(jsonToPost)) {
      throw new Error('postJsonFileToApi: Json must be an array')
    }

    for (const pathJson of jsonToPost) {
      let { url, filePath, index = -1 } = pathJson

      if (!Array.isArray(filePath)) {
        filePath = [filePath]
      }

      for (const path of filePath) {
        const urlMap = await utils.service.resolveUrl(url)
        const jsonData = await resolvers.getJsonFileData(`/json/${path}`)

        spinConfig.text = `Enviando os dados do arquivo ${path} para url ${urlMap}...`

        await postJsonToApi({ url: urlMap, json: jsonData, index })
      }
    }

    spinConfig.succeed('Envio dos dados realizado com sucesso.')
  } catch (error) {
    spinConfig.fail('Erro no envio dos dados para a nummus.')

    const err = { error, functionName: 'postJsonFileToApi' }

    await utils.report.robotErrorHandler(err)
  }
}

export const postJsonToApi = async (infoToPost) => {
  if (typeof infoToPost !== 'object' || infoToPost === null) {
    throw new Error('postJsonToApi: infoToPost must be an object')
  }

  let { url, json, index = -1 } = infoToPost
  const urlToPost = `${properties.get('url.api')}${url}`

  if (index === -1) {
    await Promise.all(json.map(async (body) => {
      const solvedJson = await resolvers.resolvePlainJson(body)

      try {
        await Axios.post(urlToPost, solvedJson, {
          headers: { empresa: properties.get('i.codigo') }
        })
      } catch (error) {
        const messageSkip = 'Você já tem uma assinatura da nummus ativa'

        const err = { error, functionName: 'postJsonToApi', messageSkip }

        await utils.report.robotErrorHandler(err)
      }
    }))
  } else if (index !== -1) {
    index = typeof index !== 'number' ? parseInt(index) : index
    const solvedJson = await resolvers.resolvePlainJson(json[index])

    try {
      await Axios.post(urlToPost, solvedJson, {
        headers: { empresa: properties.get('i.codigo') }
      })
    } catch (error) {
      const err = { error, functionName: 'postJsonToApi' }

      await utils.report.robotErrorHandler(err)
    }
  }
}

export const postJsonStringToApi = async (url, string) => {
  const urlLowerCase = url.toLowerCase()
  const urlResolved = await utils.resolver.textResolver(urlLowerCase)
  const urlMap = properties.get(urlResolved) || urlResolved
  const urlToPost = `${properties.get('url.api')}${urlMap}`

  const records = JSON.parse(string)

  for (const record of records) {
    const jsonSolved = await resolvers.resolvePlainJson(record)

    try {
      await Axios.post(urlToPost, jsonSolved, {
        headers: { empresa: properties.get('i.codigo') }
      })
    } catch (error) {
      const messageSkip = 'Você já tem uma assinatura da nummus ativa'

      const err = { error, functionName: 'postJsonStringToApi', messageSkip }

      await utils.report.robotErrorHandler(err)
    }
  }
}

export const postObjectToApi = async (url, object) => {
  if (typeof url !== 'string') throw new Error('postObjectToApi: Invalid URL!')

  const lowerCaseUrl = url.toLowerCase()
  const urlResolved = await utils.resolver.textResolver(lowerCaseUrl)
  const urlMap = properties.get(urlResolved) || urlResolved
  const urlToPost = `${properties.get('url.api')}${urlMap}`

  try {
    await Axios.post(urlToPost, object, {
      headers: { empresa: properties.get('i.codigo') }
    })
  } catch (error) {
    const err = { error, functionName: 'postObjectToApi' }

    await utils.report.robotErrorHandler(err)
  }
}

export default posting
