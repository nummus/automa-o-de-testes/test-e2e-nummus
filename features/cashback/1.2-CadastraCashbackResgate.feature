@Nummus @Cashback @Resgate
Feature: Cadastro cashbacks com resgate

  @CadastrarCashbackResgate
  Scenario Outline: Realizo cadastros de cashbacks com resgate
    Given aguardo o carregamento da página
    And aguardo por 3 segundos
    And clico no "@commons"."menuNovoCashback" caso esteja presente
    And aguardo o carregamento da página
    And clico no "@cashback"."botaoNovoCashback"
    And aguardo o carregamento da página
    And aguardo estar presente o "@cashback"."cardsDeTotal" com os seguintes textos iguais:
      | text                    |
      | TOTAL DE VENDAS R$ 0,00 |
      | VALOR A COBRAR R$ 0,00  |
      | CASHBACK R$ 0,00        |
    And aguardo estar presente o "@cashback"."avatarCliente"
    And informo o texto "<telefone>" no "@cashback"."camposCashback" na posição 0
    And aguardo estar presente o "@cashback"."camposCashback" com os seguintes valores na propriedade value iguais:
      | value            |
      | <documento>      |
      | <apelido>        |
      | <nome>           |
      | <dataNascimento> |
      | <email>          |
    And aguardo por 1 segundos
    And informo os seguintes produtos "<produto>" no cashback
    And aguardo estar presente o "@cashback"."cardTotalVenda" com os seguintes textos iguais:
      | text                            |
      | TOTAL DE VENDAS R$ <totalValor> |
    And aguardo por 1 segundos
    And clico no "@cashback"."botaoResgatar" se "<resgate>" for verdadeiro
    And informo o texto "<resgate>" no "@cashback"."campoValorResgate"
    And aguardo estar presente o "@cashback"."cardTotalCobrar" com os seguintes textos iguais:
      | text                             |
      | VALOR A COBRAR R$ <totalACobrar> |
    And aguardo estar presente o "@cashback"."cardTotalCashback" com os seguintes textos iguais:
      | text                        |
      | CASHBACK R$ <totalCashback> |
    And aguardo estar presente o "@cashback"."textoValorResgateCashback" com os seguintes textos iguais:
      | text                                        |
      | RESGATE DE CASHBACK - (33,33% SOB A COMPRA) |
    And clico no "@cashback"."botaoGerarCashback"
    And aguardo a mensagem com o texto "Cashback gerado com sucesso"
    And aguardo estar presente o "@commons"."listLine" com os seguintes textos contendo:
      | text         |
      | <linhaLista> |

    Examples:
      | telefone        | documento      | apelido | nome          | email               | dataNascimento | produto                                          | totalValor | totalCashback | resgate | totalACobrar | linhaLista                                                      |
      | (48) 99156-1063 | 054.106.372-32 | ARTHUR  | ARTHUR NUMMUS | tiago@nummus.com.br |     28/05/2000 | ../files/json/cashbacks/loopTela/produtos_1.json |     121,21 |          9,98 |   21,37 |        99,84 | ARTHUR NUMMUS R$ 121,21 R$ 21,37 R$ 0,00 R$ 99,84 R$ 9,98       |
      | (48) 99156-1063 | 054.106.372-32 | ARTHUR  | ARTHUR NUMMUS | tiago@nummus.com.br |     28/05/2000 | ../files/json/cashbacks/loopTela/produtos_2.json |     980,11 |        188,83 |   35,98 |       944,13 | ARTHUR NUMMUS R$ 980,11 R$ 35,98 R$ 0,00 R$ 944,13 R$ 188,83    |
      | (48) 99156-1063 | 054.106.372-32 | ARTHUR  | ARTHUR NUMMUS | tiago@nummus.com.br |     28/05/2000 | ../files/json/cashbacks/loopTela/produtos_3.json |   1.101,32 |        162,28 |  289,90 |       811,42 | ARTHUR NUMMUS R$ 1.101,32 R$ 289,90 R$ 0,00 R$ 811,42 R$ 162,28 |
