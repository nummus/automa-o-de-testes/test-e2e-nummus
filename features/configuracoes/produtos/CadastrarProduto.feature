@Nummus @Produto
Feature: Cadastro Produto

  @CadastrarProduto
  Scenario Outline: Realizo cadastro de produtos
    # Acesso inicial da plataforma
    Given acesso a url "produto_url" da nummus no início do scenario
    * aguardo o carregamento da página
    * aguardo estar visível o "@produto"."botaoAdicionarProduto"
    * aguardo por 2 segundos
    # Início do cadastro
    * clico no "@produto"."botaoAdicionarProduto"
    * aguardo por 2 segundos
    * informo o texto "<codigo>" no "@produto"."inputCadastroCodigoProduto"
    * informo o texto "<descricao>" no "@produto"."inputCadastroDescricaoProduto"
    * seleciono o texto contendo "<tipoValidade>" no "@produto"."selecionaTipoValidade"
    * informo o texto "<porcentagem>" no "@produto"."camposConteudoProduto" na posição 0
    * informo o texto "<validade>" no "@produto"."camposConteudoProduto" na posição 1
    * clico no botão "salvar"
    * aguardo e fecho a mensagem com o texto "Sucesso! Produto adicionado com sucesso"
    * clico no "@produto"."botaoAtualizar"
    # Validação das informações cadastradas
    * aguardo estar presente o "@commons"."listLine" com os seguintes textos iguais:
      | text         |
      | <linhaLista> |

    Examples:
      | codigo | descricao       | porcentagem | tipoValidade | validade | botaoAcaoLabel | linhaLista                               |
      | Temp1  | TestAutomaTemp1 |        1000 | Dias         |       10 | salvar         | TEMP1 TESTAUTOMATEMP1 10 dias 10,00% --- |
      | Temp2  | TestAutomaTemp2 |        1500 | Data         | 31122025 | salvar         | Temp1 TestAutomaTemp2 31/12/2025 15,00%  |
