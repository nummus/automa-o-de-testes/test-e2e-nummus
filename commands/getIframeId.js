export const command = function (position, callback = () => {}) {
  this.execute(
    (target) => {
      return document.querySelectorAll('iframe')[target].id
    },
    [position],
    ({ value }) => {
      callback(value)
    }
  )
  return this
}
